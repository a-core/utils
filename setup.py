from setuptools import setup, find_packages

setup(
    name = 'utils',
    version = '0.1.3',
    description = 'Helper tools for working with the A-Core codebase.',
    author = 'Verneri Hirvonen',
    author_email = 'verneri.hirvonen@aalto.fi',
    packages = find_packages(),
    install_requires = [
        'GitPython'
    ],
    entry_points = {
        'console_scripts': [
            'subtool = subtool.main:main'
        ]
    }
)