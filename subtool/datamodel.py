from subtool import common
import git
import os

def get_repo_name(repo):
    git_path = repo.git_dir.replace("/.git", "")
    return os.path.basename(git_path)

def traverse_preorder(node, parent, f, data=None, depth=0, last=False, **kwargs):
    """
    Do a preorder traversal of a tree starting at 'node' applying the function 'f' to each node

    Parameters
    ----------
    node :
        Current node
    parent :
        Parent node
    f : function
        Function evaluated against 'node'
    data :
        User-defined data object available at each step of the traversal
    depth : int
        Current depth in the tree
    last : bool
        True for the last child of each node
    kwargs :
        key-value arguments passed to 'f'
    """
    f(node, data, depth, last, **kwargs)
    for (i, child) in enumerate(node.children):
        traverse_preorder(child, node, f, data, depth=depth+1, last=(i==len(node.children)-1), **kwargs)

def _collect_names(node, data, depth, last, **kwargs):
    data.append(_submodule_relpath(node))

def _collect_depths(node, data, depth, last, **kwargs):
    data.append(depth)

def _collect_heads(node, data, depth, last, **kwargs):
    data.append(_head_str(node))

def _submodule_relpath(node):
    if node.parent is not None:
        return os.path.relpath(node.data['repo'].working_dir, node.parent.data['repo'].working_dir)
    else:
        return node.data['name']

def _submodule_hash(node):
    """ short git hash of a submodule target as seen from the parent """
    arrow = "-> "
    if node.parent is None:
        return (len(arrow)+7)*" "
    else:
        submod_hash = node.parent.data['repo'].submodule(_submodule_relpath(node)).hexsha
        short_hash = submod_hash[0:7]
        return arrow + common.cyellow + short_hash + common.cend

def _commit_hash(node):
    """ short git has of commit pointed to by current HEAD """
    return common.cyellow + node.data['repo'].commit("HEAD").hexsha[0:7] + common.cend

def _head_str(node):
    """ string representation of node HEAD reference """
    try:
        return common.cgreen + node.data['repo'].head.ref.name + common.cend
    except TypeError as e:
        commit = node.data['repo'].head.commit
        return "%sHEAD detached%s" % (common.cred, common.cend)

def _node_str(node, data, depth, last, **kwargs):
    max_len_name = kwargs.get('max_len_name')
    max_len_head = kwargs.get('max_len_head')
    max_depth = kwargs.get('max_depth')
    name_pad = (max(10, max_len_name) - len(_submodule_relpath(node)) + 2*(max_depth-depth))*" "
    head_pad = (max(7, max_len_head) - len(_head_str(node)))*" "

    submod_hash = _submodule_hash(node)
    head_hash = _commit_hash(node)
    head_str = _head_str(node)
    modified = "*" if depth > 0 and submod_hash[3:] != head_hash else ""

    if depth > 0:
        sym = "└" if last else "├"
        data.append((depth-1)*"│ "+f"{sym}─")
    data.append("%s%s  %s  %s%s at %s%s\n"
        % (_submodule_relpath(node), name_pad, submod_hash, head_str, head_pad, head_hash, modified))

class Tree:
    def __init__(self, parent, data=None):
        # tree edges
        self.parent = parent
        self.children = []
        # node data
        self.data = {}
        if data is not None:
            self.data = data
    
    def __str__(self):
        names = []
        traverse_preorder(node=self, parent=None, f=_collect_names, data=names)
        max_len_name = max(map(len, names))

        depths = []
        traverse_preorder(node=self, parent=None, f=_collect_depths, data=depths)
        max_depth = max(depths)

        heads = []
        traverse_preorder(node=self, parent=None, f=_collect_heads, data=heads)
        max_len_head = max(map(len, heads))


        # FIXME: compute header pads from content length
        header_repo = "Repository"
        header_repo_pad = (max(max_len_name+max_depth*2 - len(header_repo), 0)+1)*" "
        header = "%s%s Submodule%sHEAD\n" % ((header_repo, header_repo_pad, "   "))

        # key-value args passed to _node_str
        kwargs = {
            "max_len_name": max_len_name,
            "max_len_head": max_len_head,
            "max_depth": max_depth,
        }
        segments = [header]
        traverse_preorder(node=self, parent=None, f=_node_str, data=segments, **kwargs)

        # insert header rules
        hrule = 80*"─"+"\n"
        segments.insert(0, hrule)
        segments.insert(2, hrule)

        return ''.join(segments)
    
    @staticmethod
    def frm(repo): # from
        def _recurse_submodules(parent, node, repo, depth=0):
            # populate self
            node.data['name'] = get_repo_name(repo)
            node.data['repo'] = repo

            # recursively iterate submodules
            for submod in repo.submodules:
                try:
                    submod_repo = git.Repo(submod.abspath)
                except git.InvalidGitRepositoryError as e:
                    common.print_fatal(
                        "uninitialized submodule: %s" % (submod.abspath),
                        "initialize repository submodules first with: 'git submodule update --init --recursive'")
                submod_node = Tree(parent=node)
                node.children.append(submod_node)
                _recurse_submodules(node, submod_node, submod_repo, depth+1)
        
        root = Tree(parent=None)
        _recurse_submodules(None, root, repo)
        return root
