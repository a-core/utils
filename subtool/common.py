# ANSI escape sequences for terminal colors
cend    = '\33[0m'
cblack  = '\33[30m'
cred    = '\33[31m'
cgreen  = '\33[32m'
cyellow = '\33[33m'
cblue   = '\33[34m'
cviolet = '\33[35m'
cbeige  = '\33[36m'
cwhite  = '\33[37m'

def print_fatal(*lines):
    message = "[%serror%s]: %s\n" % (cred, cend, lines[0])
    for line in lines[1:]:
        message += "%s\n" % (indent_block(line, 9))
    print(message, end='')
    exit(-1)

def print_warning(*lines):
    message = "[%swarn%s]: %s\n" % (cyellow, cend, lines[0])
    for line in lines[1:]:
        message += "%s\n" % (indent_block(line, 8))
    print(message, end='')

def print_info(*lines):
    message = "[%sinfo%s]: %s\n" % (cwhite, cend, lines[0])
    for line in lines[1:]:
        message += "%s\n" % (indent_block(line, 8))
    print(message, end='')

def print_verbose(enable=False, *lines):
    for line in lines:
        if enable:
            print(line)

def indent_block(block, width):
    spaces = width*" "
    return spaces + block.replace("\n", "\n"+spaces)
