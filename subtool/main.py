#!/usr/bin/env python3

import os
import git
from subtool import common
from subtool.datamodel import *
import argparse


def main():
    # top-level parser
    parser = argparse.ArgumentParser()
    parser.add_argument('--verbose', action='store_true', default=False, help='Print verbose output')
    parser.add_argument('--root-dir', help='Root directory of your project repository hierarchy. Default: $PWD')
    subparsers = parser.add_subparsers(dest='subcommand', required=True)

    # tree subcommand parser
    tree_parser = subparsers.add_parser('tree', help='Print submodule hierarchy')

    # sync subcommand parser
    sync_parser = subparsers.add_parser('sync', help='Synchronize submodule versions')
    sync_parser.add_argument('submodule', help='Submodule name')
    sync_parser.add_argument('branch', help='Working branch')

    # push subcommand parser
    push_parser = subparsers.add_parser('push', help='Push branches from the local hierarchy to remote.')
    push_parser.add_argument('submodule')

    args = parser.parse_args()

    if args.root_dir is None:
        args.root_dir = os.getcwd()
        if (args.verbose):
            print("Using working directory as root repository: %s" % (args.root_dir))
    args.root_dir = os.path.abspath(args.root_dir)

    if args.subcommand == 'tree':
        tree(args)
    elif args.subcommand == 'sync':
        synchronize(args)
    elif args.subcommand == 'push':
        push(args)
    else:
        raise RuntimeError("Invalid command %s" % (args.command))


def _get_relative_path(node):
    def _get_relative_path_helper(path, node):
        if node.parent.parent is None:
            return "./" + path
        return _get_relative_path_helper(node.parent.data['name'] + "/" + path , node.parent)
    if node.parent is None:
        return "."
    else:
        return _get_relative_path_helper(node.data['name'], node)

def _get_submodule_nodes(node, data, submodule):
    """ construct a function that looks for submodule by name """
    def visit(node, data, depth, last):
        if node.data['name'] == submodule:
            data.append(node)
    return visit

def _validate_submodule_exists(nodes, name):
    """ make sure that at least one submodule has the given name """
    for node in nodes:
        if node.data['name'] == name:
            return True
    return False

def _validate_branch_exists(nodes, branch):
    """ make sure that at least one branch in any submodule as the given name """
    branch_ref = 'refs/heads/%s' % (branch)
    for node in nodes:
        if branch_ref in [b.path for b in node.data['repo'].branches]:
            return True
    return False

def _get_sync_source_submodule(nodes, branch):
    """ determine which submodule to use as the synchronization source by comparing commit times """
    # collect nodes that have target branch
    nodes_with_branch = []
    branch_ref = 'refs/heads/%s' % (branch)
    for node in nodes:
        if branch_ref in [b.path for b in node.data['repo'].branches]:
            nodes_with_branch.append(node)

    # compare commit timestamps in submodule instances containing the target branch
    latest_node = nodes_with_branch[0]
    for node in nodes_with_branch:
        try:
            if node.data['repo'].commit(branch).committed_datetime > latest_node.data['repo'].commit(branch).committed_datetime:
                latest_node = node
        except:
            pass
    return latest_node

def _validate_empty_index(node):
    """ make sure that the submodule instance has uncommitted changes in their staging area """
    if len(node.data['repo'].index.diff("HEAD")) != 0:
        common.print_fatal("staging area of '%s' contains uncommitted work" % (_get_relative_path(node)))

def _update_parents(node, branch, verbose=False):
    def _update_helper(node, branch, verbose):
        # cannot update parent of root node
        parent = node.parent
        if parent is None:
            return

        common.print_verbose(verbose, "    %s:" % (_get_relative_path(parent)))

        # require clean index
        _validate_empty_index(parent)

        common.print_verbose(verbose, "      - fetching from origin")
        parent.data['repo'].git.fetch()

        # look for target branch
        branch_found = False
        for ref in parent.data['repo'].refs:
            if ref.name == branch or ref.name == "origin/" + branch:
                branch_found = True
                break

        # [create], checkout branch
        if not branch_found:
            common.print_verbose(verbose, "      - creating branch '%s'" % (branch))
            result = parent.data['repo'].git.branch(branch)
            if verbose:
                common.print_verbose(verbose, common.indent_block(result, 6))
        common.print_verbose(verbose, "      - checking out branch '%s'" % (branch))
        result = parent.data['repo'].git.checkout(branch)
        if verbose:
            common.print_verbose(verbose, common.indent_block(result, 6))

        # update submodule pointer
        common.print_verbose(verbose, "      - updating submodule pointer")

        # check if submodule pointer needs to be updated
        submod_staus = parent.data['repo'].git.submodule("status", node.data['name'])
        if submod_staus[0] == "+":
            parent.data['repo'].git.add(node.data['name'])
            parent.data['repo'].git.commit("-m", "subtool sync: update %s" % (node.data['name']))

        _update_helper(parent, branch, verbose)

    _update_helper(node, branch, verbose)


# subcommands
def tree(args):
    """
    Print submodule hierarchy tree
    """
    try:
        root_repo = git.Repo(args.root_dir)
    except git.InvalidGitRepositoryError as e:
        common.print_fatal("not a git repository: %s" % (args.root_dir))
    tree = Tree.frm(root_repo)
    if len(tree.children) == 0:
        common.print_warning("no submodules in repository: %s" % (args.root_dir))
    print(tree, end='')

def synchronize(args):
    """
    Synchronize all submodules 
    """

    try:
        root_repo = git.Repo(args.root_dir)
    except git.InvalidGitRepositoryError as e:
        common.print_fatal("not a git repository: %s" % (args.root_dir))
    tree = Tree.frm(root_repo)

    common.print_verbose(args.verbose, "arguments:",
        "  * name='%s'" % (args.submodule),
        "  * branch='%s'" % (args.branch))
    common.print_info("initial state:")
    common.print_verbose(True, str(tree))

    # collect nodes that are instances of the target submodule
    nodes = []
    traverse_preorder(tree, parent=None, f=_get_submodule_nodes(tree, nodes, args.submodule), data=nodes)

    # validate arguments against submodule tree
    if not _validate_submodule_exists(nodes, args.submodule):
        common.print_fatal("no such submodule '%s'" %(args.submodule))
    if not _validate_branch_exists(nodes, args.branch):
        common.print_fatal("no such branch '%s'" %(args.branch))

    # require clean index
    for node in nodes:
        _validate_empty_index(node)
    
    # look up and separate most up-to-date submodule instance from the others
    sync_node = _get_sync_source_submodule(nodes, args.branch)
    nodes.remove(sync_node)
    common.print_info("using '%s' as the sync source" % (_get_relative_path(sync_node)))

    # push sync source to remote for others to pull
    common.print_verbose(args.verbose, "  * pushing branch '%s' to origin" % (args.branch))
    sync_node.data['repo'].git.push('origin', args.branch)

    # pull changes on other submodule instances
    common.print_info("updating other instances of '%s'" % (args.submodule))
    for node in nodes:
        if args.verbose:
            common.print_verbose(args.verbose, "  %s:" % (_get_relative_path(node)))
            common.print_verbose(args.verbose, "    - fetching updates from origin")
        node.data['repo'].git.fetch()
        if args.verbose:
            common.print_verbose(args.verbose, "    - switching to branch '%s'" % (args.branch))
        node.data['repo'].git.checkout(args.branch)
        origin = node.data['repo'].remote(name='origin')
        if args.verbose:
            common.print_verbose(args.verbose, "    - pulling latest changes for branch '%s'" % (args.branch))
        fetch_info = origin.pull(args.branch)

    # update parent node submodule pointers, create branches as needed
    common.print_info("updating submodules in parent repositories")
    for node in nodes:
        common.print_verbose(args.verbose, "  %s:" % (_get_relative_path(node)))
        _update_parents(node, args.branch, args.verbose)
    common.print_verbose(args.verbose, "  %s:" % (_get_relative_path(sync_node)))
    _update_parents(sync_node, args.branch, args.verbose)
    
    # parse tree again to update branch names
    common.print_info("synchronized state:")
    common.print_verbose(True, str(tree))

def push(args):
    """
    Push submodule repositories and their parents
    """
    raise NotImplementedError("push: subcommand has not yet been implemented!")


if __name__=='__main__':
    main()
