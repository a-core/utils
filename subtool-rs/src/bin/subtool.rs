use std::env::current_dir;

use clap::{Parser, Subcommand};
use subtool::{commands, path::RepoPath};

#[derive(Parser, Debug)]
#[command(author, about, long_about = None)]
struct Args {
    #[command(subcommand)]
    command: Commands,

    /// Print verbose output
    #[arg(long)]
    verbose: bool,

    /// Root directory of your project repository hierarchy. Default: $PWD
    #[arg(long)]
    root_dir: Option<String>,
}

#[derive(Subcommand, Debug)]
enum Commands {
    /// Print submodule hierarchy
    Tree,

    /// Synchronize submodule versions
    Sync {
        /// Submodule name
        submodule: String,

        /// Working branch
        branch: String,
    },

    /// Push branches from the local hierarchy to remote
    Push,
}

fn main() {
    let args = Args::parse();
    //println!("{args:#?}");

    // default to $PWD if `--root-dir` is not provided
    let mut root_dir = current_dir().expect("Could not determine current directory");
    if let Some(path) = args.root_dir {
        root_dir = path.into();
    }

    // parse RepoPath from root_dir
    let root_dir = match RepoPath::try_from(root_dir.as_ref()) {
        Ok(path) => path,
        Err(e) => {
            eprintln!(
                "Invalid repository root path: {}\n- {}",
                root_dir.as_path().to_str().unwrap(),
                e
            );
            std::process::exit(-1);
        }
    };

    match &args.command {
        Commands::Tree => {
            commands::tree(&root_dir);
        }
        Commands::Sync { submodule, branch } => {
            commands::sync();
        }
        Commands::Push => {
            commands::push();
        }
    }
}
