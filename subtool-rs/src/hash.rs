use git2::Oid;

#[derive(Debug)]
pub struct GitHash {
    pub oid: Oid,
}
impl GitHash {
    pub fn short_string(&self) -> String {
        let hash = format!("{}", self.oid);
        hash[0..7].to_string()
    }
}
impl PartialEq for GitHash {
    fn eq(&self, other: &Self) -> bool {
        self.oid == other.oid
    }
}
impl From<Oid> for GitHash {
    fn from(oid: Oid) -> Self {
        Self { oid }
    }
}
