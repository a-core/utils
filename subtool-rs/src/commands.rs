use crate::{path::RepoPath, tree::RepoTree};
pub fn tree(root_dir: &RepoPath) {
    // try to parse repository hierarchy from provided path
    let repo_tree = match RepoTree::try_from(root_dir) {
        Ok(tree) => tree,
        Err(e) => match e.code() {
            _ => panic!("{:#?}", e),
        },
    };

    // print repository hierarchy in human readable format
    println!("{repo_tree}");

    // dump debug view of tree structure
    // println!("{repo_tree:#?}");
}

pub fn sync() {
    todo!();
}

pub fn push() {
    todo!();
}
