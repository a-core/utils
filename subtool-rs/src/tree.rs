use git2::{Error as GitError, Oid, Repository, Submodule};
use std::{
    cell::RefCell,
    fmt::{Debug, Display},
    path::PathBuf,
    rc::Rc,
};

use crate::{hash::GitHash, path::RepoPath};

#[derive(Debug)]
pub enum HeadRef {
    Detached,
    Branch(String),
}

#[derive(Debug)]
enum RepoStatus {
    /// Submodule commit is missing from git tree object
    /// associated with HEAD of current repository. This can happen
    /// if someone manually modifies .gitmodules instead of adding
    /// the submodule using `git submodule add`.
    MissingFromTree,

    /// The given submodule has not been initialized yet.
    Uninitialized,

    /// A valid git submodule.
    Ok {
        path: RepoPath,
        origin: String,
        head_ref: HeadRef,
        head_commit: GitHash,
        children: Vec<RepoTree>,
    },
}

#[derive(Debug)]
/// Node type for a repository hierarchy tree data type
pub struct RepoTree {
    name: String,
    parent_commit: Option<GitHash>,
    status: RepoStatus,
}

impl RepoTree {
    /// Build RepoTree representation starting at given root directory
    ///
    /// Arguments:
    /// * `root_dir` well-formed path to a directory inside a git repository
    /// * `parent_ref` HEAD ref (branch) of parent repository
    /// * `parent_commit` submodule commit pointed to by the parent repository
    pub fn new(root_dir: &RepoPath, parent_commit: Option<GitHash>) -> Result<Self, GitError> {
        let name = root_dir
            .as_ref()
            .file_name()
            .unwrap()
            .to_str()
            .unwrap()
            .to_string();
        let repo = Repository::open(root_dir).unwrap();

        let submodules = repo.submodules().unwrap();
        let children = submodules
            .iter()
            .map(|sm| RepoTree::from_submodule(&repo, root_dir, sm))
            .collect();

        let origin = repo.find_remote("origin")?;
        let origin_url = origin.url().unwrap().to_string();

        let head = &repo.head()?;
        let commit = repo.find_commit(head.target().unwrap())?;

        let head_ref = if repo.head_detached()? {
            HeadRef::Detached
        } else {
            HeadRef::Branch(head.shorthand().unwrap().to_string())
        };

        let head_commit = GitHash::from(commit.id());

        Ok(RepoTree {
            name,
            parent_commit,
            status: RepoStatus::Ok {
                path: root_dir.clone(),
                origin: origin_url,
                head_ref,
                head_commit,
                children,
            },
        })
    }

    /// Defining an explicit method instead of implementing From because we need
    /// to pass some context information to this method.
    ///
    /// Arguments:
    /// * `repo` [git2::Repository] of parent repository
    /// * `parent_path` [RepoPath] pointing to parent repository root directory.
    /// * `submod` [git2::Submodule] from which to
    fn from_submodule(repo: &Repository, parent_path: &RepoPath, submod: &Submodule) -> Self {
        // name as seen by parent repository
        let submodule_name = match submod.name() {
            Some(s) => s.to_string(),
            None => "<unknown submodule name>".to_string(),
        };

        let submod_commit = match RepoTree::submodule_commit(repo, submod) {
            Ok(commit) => commit,
            Err(_) => {
                return RepoTree {
                    name: submodule_name,
                    parent_commit: None,
                    status: RepoStatus::MissingFromTree,
                }
            }
        };
        let submod_commit = GitHash::from(submod_commit);

        // if submodule is not initialized, parsing to repopath gives path to parent module
        let submod_abspath: PathBuf = [parent_path.as_ref(), submod.path()].iter().collect();
        let submod_repopath = RepoPath::try_from(submod_abspath.as_ref()).unwrap();
        if submod_abspath != submod_repopath.as_ref() {
            return RepoTree {
                name: submodule_name,
                parent_commit: Some(submod_commit),
                status: RepoStatus::Uninitialized,
            };
        }

        let mut submod_repo = match RepoTree::new(&submod_repopath, Some(submod_commit)) {
            Ok(repo) => repo,
            Err(e) => {
                eprintln!("Could not open submodule as git repository. Try initializing submodules first:\n{e}");
                std::process::exit(-1);
            }
        };

        // use submodule name from parent instead of actual repository name
        submod_repo.name = submodule_name;
        submod_repo
    }

    /// Determine submodule commit hash
    fn submodule_commit(repo: &Repository, submod: &Submodule) -> Result<Oid, GitError> {
        let submod_relpath = PathBuf::from(submod.name().unwrap());
        let head = repo.head()?;
        let commit = repo.find_commit(head.target().unwrap())?;
        let tree = commit.tree()?;
        let submod_commit = tree.get_path(submod_relpath.as_path())?;
        Ok(submod_commit.id())
    }

    /// Traverse repository hierarchy tree in given order starting from current node
    ///
    /// Arguments:
    /// * `order` specifies the order of traversal
    /// * `data` mutable reference to a variable accessible from all nodes
    /// * `funct` function to be evaluated against each node
    ///
    /// `funct` is a closure that takes two arguments:
    /// 1. `&TraversalContext` that contains tree traversal metadata, and
    /// 2. `&Self` reference to currently visited node
    pub fn traverse<F, T>(&self, order: &TraversalOrder, data: TraversalData<T>, funct: &mut F)
    where
        F: FnMut(&TraversalContext, &RepoTree, TraversalData<T>),
        T: Clone,
    {
        // init traversal context and call recursively call traverse_helper
        let cx = TraversalContext::init();
        self.traverse_helper(order, data, funct, cx);
    }

    fn traverse_helper<F, T>(
        &self,
        order: &TraversalOrder,
        data: TraversalData<T>,
        funct: &mut F,
        cx: TraversalContext,
    ) where
        F: FnMut(&TraversalContext, &RepoTree, TraversalData<T>),
        T: Clone,
    {
        match &order {
            TraversalOrder::Inorder => {
                // visit current node
                funct(&cx, self, data.clone());

                // traverse children of initialized submodules
                if let RepoStatus::Ok { children, .. } = &self.status {
                    for (i, child) in children.iter().enumerate() {
                        let is_last = i == children.len() - 1;
                        let mut cx = cx.clone();
                        cx.depth += 1;
                        cx.last_parents.push(Some(is_last));
                        child.traverse_helper(order, data.clone(), funct, cx);
                    }
                }
            }
        }
    }
}
impl TryFrom<&RepoPath> for RepoTree {
    type Error = GitError;

    fn try_from(root_dir: &RepoPath) -> Result<Self, Self::Error> {
        RepoTree::new(root_dir, None)
    }
}

//type TraversalData<T> = Rc<RefCell<T>>;
/// Enumeration encoding an optional shared reference to a generic type
/// used in tree traversal. This can be used by the node visitor function
/// to store information about the currently visited node.
#[derive(Debug, Clone)]
pub enum TraversalData<T: Clone> {
    None,
    Some(Rc<RefCell<T>>),
}
impl<T: Clone + Debug> TraversalData<T> {
    /// Return contained value
    ///
    /// # Panics
    /// Panics if the self value equals [`None`] or if the underlying Rc is not
    /// the only pointer to the inner value.
    pub fn unwrap(self) -> T {
        match self {
            TraversalData::None => panic!(),
            TraversalData::Some(rc) => {
                let refcell =
                    Rc::try_unwrap(rc).expect("Stored Rc is not the only pointer to shared data");
                refcell.into_inner()
            }
        }
    }
}
impl<T: Clone> From<T> for TraversalData<T> {
    fn from(f: T) -> Self {
        TraversalData::Some(Rc::new(RefCell::new(f)))
    }
}

pub enum TraversalOrder {
    Inorder,
}

#[derive(Clone)]
/// Stores tree traversal metadata
/// * `depth` of current node in the tree
/// * `last_parents` elements correspond to parent nodes until the root
///   of the tree and indicate whether the given node was the last child
///   (rightmost) of its parent node.
pub struct TraversalContext {
    pub depth: usize,
    pub last_parents: Vec<Option<bool>>,
}
impl TraversalContext {
    /// Inital value for TraversalContext
    pub fn init() -> Self {
        TraversalContext {
            depth: 0,
            last_parents: vec![None],
        }
    }
}

mod printer {
    use colored::Colorize;
    use itertools::Itertools;

    use super::{RepoStatus, RepoTree, TraversalContext, TraversalData, TraversalOrder};

    /// Count number of bytes in a string belonging to ANSI escape sequences
    fn count_ansi_bytes(s: &str) -> usize {
        let initial_len = s.len();
        let replaced_len = parse_ansi::ANSI_REGEX
            .replace_all(s.as_bytes(), b"" as &[u8])
            .len();
        initial_len - replaced_len
    }

    /// TODO:
    /// - format string needs to be a string literal so it needs to be repeated.
    ///   could it be extracted into a macro?
    pub fn output_lines(repo_tree: &RepoTree) -> Vec<String> {
        // determine length of tree view column
        let mut prefixed_lengths: Vec<usize> = prefixed_names(repo_tree)
            .iter()
            .map(|s| s.chars().count() - count_ansi_bytes(s))
            .collect();
        prefixed_lengths.push(10); // edge case: pad to header length if sm name is short
        let name_column_len = prefixed_lengths.iter().max().unwrap();

        let mut lines: Vec<String> = Vec::new();
        lines.push(
            format!(
                "{:<name_column_len$} {:10}  {}",
                "Repository", "Submodule", "HEAD"
            )
            .bold()
            .to_string(),
        );

        let working_data = TraversalData::from(lines);
        repo_tree.traverse(
            &TraversalOrder::Inorder,
            working_data.clone(),
            &mut |cx, repo, data| {
                if let TraversalData::Some(lines) = data {
                    let ansi_offset_name_column_len =
                        name_column_len + count_ansi_bytes(&prefixed_name(cx, repo));
                    let submodule_arrow = match repo.parent_commit {
                        Some(_) => "->",
                        None => "  ",
                    };
                    let parent_commit_str = match &repo.parent_commit {
                        Some(hash) => hash.short_string(),
                        None => "".to_string(),
                    };
                    match &repo.status {
                        RepoStatus::Ok {
                            head_ref,
                            head_commit,
                            ..
                        } => {
                            let head = match head_ref {
                                super::HeadRef::Detached => "HEAD detached".red(),
                                super::HeadRef::Branch(b) => b.magenta(),
                            };
                            lines.borrow_mut().push(format!(
                                "{:<ansi_offset_name_column_len$} {} {:7}  {} at {}",
                                prefixed_name(cx, repo),
                                submodule_arrow,
                                parent_commit_str.yellow(),
                                head,
                                head_commit.short_string().yellow(),
                            ));
                        }
                        RepoStatus::Uninitialized => lines.borrow_mut().push(format!(
                            "{:<ansi_offset_name_column_len$} {} {:7}  {}",
                            prefixed_name(cx, repo),
                            submodule_arrow,
                            parent_commit_str.yellow(),
                            "uninitialized".red()
                        )),
                        RepoStatus::MissingFromTree => lines.borrow_mut().push(format!(
                            "{:<ansi_offset_name_column_len$} {} {:7}  {}",
                            prefixed_name(cx, repo),
                            submodule_arrow,
                            "missing".red(),
                            "Submodule target commit missing from tree!".red()
                        )),
                    }
                }
            },
        );
        working_data.unwrap()
    }

    /// Generate tree structure visualization from traversal state
    fn tree_prefix(state: &TraversalContext) -> String {
        let mut prefix = String::new();
        if state.depth > 1 {
            let boolvec: Vec<&bool> = state.last_parents.iter().flatten().collect();
            let pipes: String = boolvec
                .iter()
                .dropping_back(1)
                .map(|x| match x {
                    true => "  ",
                    false => "│ ",
                })
                .collect();
            prefix.push_str(&pipes);
        }
        if state.depth > 0 {
            // FIXME: last parents last unwrap might panic
            match state.last_parents.last().unwrap().unwrap_or(false) {
                true => prefix.push_str("└─"),
                false => prefix.push_str("├─"),
            };
        }
        prefix
    }

    fn prefixed_name(cx: &TraversalContext, repo: &RepoTree) -> String {
        let modified_marker = match &repo.status {
            RepoStatus::Ok { head_commit, .. } => {
                if let Some(parent_commit) = &repo.parent_commit {
                    if head_commit == parent_commit {
                        ""
                    } else {
                        " *"
                    }
                } else {
                    ""
                }
            }
            _ => "",
        };
        let uninit = match repo.status {
            RepoStatus::Ok { .. } => "".to_string(),
            RepoStatus::Uninitialized | RepoStatus::MissingFromTree => " !".red().to_string(),
        };
        format!(
            "{}{}{}{}",
            tree_prefix(cx).cyan(),
            repo.name,
            uninit,
            modified_marker.bold()
        )
    }

    /// collect a vector of submodule names with their corresponding tree structure prefixes
    fn prefixed_names(repo_tree: &RepoTree) -> Vec<String> {
        let prefixed_names: Vec<String> = Vec::new();
        let working_data = TraversalData::from(prefixed_names);
        repo_tree.traverse(
            &TraversalOrder::Inorder,
            working_data.clone(),
            &mut |cx, repo, data| {
                if let TraversalData::Some(data) = data {
                    data.borrow_mut().push(prefixed_name(cx, repo));
                }
            },
        );
        working_data.unwrap()
    }
}

impl Display for RepoTree {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let lines = printer::output_lines(self);
        write!(f, "{}", lines.join("\n"))
    }
}
