use std::path::{Path, PathBuf};

use git2::Repository;

/// Path to a git repository that is guaranteed to point to the
/// root directory of a git repository.
#[derive(Debug, Clone)]
pub struct RepoPath(PathBuf);

impl TryFrom<&Path> for RepoPath {
    type Error = &'static str;

    /// Try to parse a path representation that is guaranteed to point into the root of a git repository
    ///
    /// Arguments:
    /// * `path` well-formed path to a directory inside a git repository
    fn try_from(path: &Path) -> Result<Self, Self::Error> {
        // Try to open repository. git2::Repository expects root_dir the root directory of a repository
        // so passing a subdirectory inside a git submodule will fail. In this case walk up directories
        // in `root_path` until:
        // 1. a valid git repository root is found, or
        // 2. we reach the root directory / without finding a repository
        let mut abspath = path.canonicalize().unwrap();
        loop {
            match Repository::open(&abspath) {
                Ok(_repo) => return Ok(RepoPath(abspath)),
                Err(_e) => {
                    if !abspath.pop() {
                        return Err("Given path is not inside a git repository");
                    }
                }
            };
        }
    }
}
impl AsRef<Path> for RepoPath {
    fn as_ref(&self) -> &Path {
        self.0.as_path()
    }
}
