# subtool-rs

`subtool` is a helper utility for working with large git submodule hierarchies.

This crate reimplements `subtool` in Rust without any of the python version incompatibility issues.

## Installation
1. Install [Rust](https://www.rust-lang.org/tools/install) if it isn't installed already. You can check if an existing installation by running `$ cargo --version`.
2. Install `subtool-rs` using `Cargo` by running the following command inside this directory.
```shell
$ cargo install --path .
```
3. Add `~/.cargo/bin` to `PATH`.

## Examples
Currently, only the `tree` subcommand is supported.
```shell
$ subtool tree
```
